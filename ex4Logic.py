class HeaterLogic:
    # Retourne la température actuelle
    def get_actual_temp(self):
        pass

    # Retourne la liste des température des 7 derniers jours
    def get_last_days_temps(self):
        pass

    # Allume ou éteint le chauffage de la piscine en fonction du booléen
    def set_heater(self, power):
        pass
