from unittest import TestCase
from unittest.mock import MagicMock

from evaluation_tests.ex4Heater import HeaterAuto


class TestHeater(TestCase):

    def test_conditions_all_ok(self):

        mock = MagicMock()
        mock.get_actual_temp.return_value = 26
        mock.get_last_days_temps.return_value = [20, 20, 20, 20, 21, 22, 23]
        heater = HeaterAuto(heater_Logic=mock)

        heater.turn_on_heater()
        self.assertTrue(heater.powered_heater)

    def test_conditions_actTemp_only_ok(self):

        mock = MagicMock()
        mock.get_actual_temp.return_value = 26
        mock.get_last_days_temps.return_value = [19, 20, 20, 20, 17, 22, 18]
        heater = HeaterAuto(heater_Logic=mock)

        heater.turn_on_heater()
        self.assertFalse(heater.powered_heater)

    def test_conditions_oldTemp_only_ok(self):

        mock = MagicMock()
        mock.get_actual_temp.return_value = 23
        mock.get_last_days_temps.return_value = [20, 20, 20, 20, 21, 22, 23]
        heater = HeaterAuto(heater_Logic=mock)

        heater.turn_on_heater()
        self.assertFalse(heater.powered_heater)

    def test_conditions_none_ok(self):

        mock = MagicMock()
        mock.get_actual_temp.return_value = 23
        mock.get_last_days_temps.return_value = [19, 20, 20, 20, 17, 22, 18]
        heater = HeaterAuto(heater_Logic=mock)

        heater.turn_on_heater()
        self.assertFalse(heater.powered_heater)

