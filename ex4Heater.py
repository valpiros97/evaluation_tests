from evaluation_tests.ex4Logic import HeaterLogic


class HeaterAuto:
    powered_heater = False
    last_days_temp = []
    act_temp = 0

    def __init__(self, heater_Logic: HeaterLogic) -> None:
        super().__init__()
        self.heater_Logic = heater_Logic

    def turn_on_heater(self):
        self.last_days_temp = self.heater_Logic.get_last_days_temps()
        self.act_temp = self.heater_Logic.get_actual_temp()

        # set_heater will get True if condition is validated
        self.powered_heater = sum(self.last_days_temp) / len(self.last_days_temp) > 20 and self.act_temp > 25
        self.heater_Logic.set_heater(self.powered_heater)
