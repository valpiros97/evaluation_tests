from unittest import TestCase


# mettre bout à bout (join) une liste de strings en utilisant un séparateur donné.
def join_string_list(list, sep=" "):
    # default separator is space
    string = ""
    for word in list:
        string = string + word + sep
    return string.rstrip(sep)


# Calculer la moyenne d’une liste d’entier
def average_from_list(list):
    total = 0
    for number in list:
        total += number
    return total / len(list)


class TestEx1(TestCase):

    def test_join_with_space(self):
        self.assertEqual("hello world", join_string_list(["hello", "world"], " "))

    def test_join_with_special(self):
        self.assertEqual("hello@world", join_string_list(["hello", "world"], "@"))

    def test_join_lot_of_words(self):
        self.assertEqual("hello world how are you going",
                         join_string_list(["hello", "world", "how", "are", "you", "going"], " "))

    def test_join_if_no_separator_chosen(self):
        self.assertEqual("hello world", join_string_list(["hello", "world"]))

    def test_average_decimal_from_list(self):
        self.assertEqual(10, average_from_list([5, 10, 15]))

    def test_average_rounded_from_list(self):
        self.assertAlmostEqual(10.33, average_from_list([9, 10, 12]), delta=0.01)

    def test_average_error_from__not_number_list(self):
        with self.assertRaises(TypeError):
            average_from_list(["hello", "world"])

    def test_average_error_from__empty_list(self):
        with self.assertRaises(ZeroDivisionError):
            average_from_list([])