from unittest import TestCase

def auto_next_line (string, max_lenght, max_word_lenght):
    final_string = ""
    for word in string.split(" "):
        if len(word) > max_lenght:
            raise Exception("A word is too long for the max lenght given")
        actual_line = ""
        # get the actual line, for actual length
        for line in final_string.split("\n"):
            actual_line = line
        # if start of line, no need the " " character
        if actual_line == "":
            final_string = final_string + word
        else:
            if len(actual_line) + 1 + len(word) <= max_lenght:
                final_string = final_string + " " + word
            # a long word can be cut in half in end of line, but it needs at least 3 free char for " " + 1st char + "-"
            elif len(word) >= max_word_lenght and len(actual_line) <= (max_lenght - 3):
                # index cut : max lenght - actual index in line - 1 for " " after previous word - 1 for "-" character
                pre_word = word[:(max_lenght - len(actual_line) - 2)]
                past_word = word[(max_lenght - len(actual_line)) - 2:]
                final_string = final_string + " " + pre_word + "-\n" + past_word
            else:
                final_string = final_string + "\n" + word
    return final_string


class TestNextLine(TestCase):

    def test_simple_next_line(self):
        self.assertEqual("This is a\nTest", auto_next_line("This is a Test", 10, 5))

    def test_limit_next_line(self):
        self.assertEqual("This is a\nTest", auto_next_line("This is a Test", 9, 5))

    def test_limit2_next_line(self):
        self.assertNotEqual("This is aa\nTest", auto_next_line("This is a Test", 9, 5))

    def test_long_next_line(self):
        self.assertEqual("This is a\nTest string\nwhich is way\ntoo long", auto_next_line("This is a Test string which is way too long", 12, 5))

    def test_already_n_next_line(self):
        self.assertEqual("This is\na Test", auto_next_line("This is\n a Test", 10, 5))

    def test_too_long_word(self):
        with self.assertRaises(Exception):
            auto_next_line("This is a Testimonial", 10)

    def test_big_word_normal(self):
        self.assertEqual("This is an\nlongword\neasy", auto_next_line("This is an longword easy", 10, 5))

    def test_big_word_at_end_of_line(self):
        self.assertEqual("This is an\neasy long-\nword", auto_next_line("This is an easy longword", 10, 5))

    def test_big_word_at_end_of_line_limit(self):
        self.assertEqual("This is l-\nongword\neasy", auto_next_line("This is longword easy", 10, 5))
