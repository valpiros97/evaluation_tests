from unittest import TestCase


def is_leap_year_v1(year):
    # Toutes les années divisibles par 400 sont bissextiles
    if year % 400 == 0:
        return True
    else:
        return False


def is_leap_year_v2(year):
    # Toutes les années divisibles par 400 sont bissextiles
    if year % 400 == 0:
        return True
    # Toutes les années divisibles par 100 mais pas par 400 ne sont pas bissextiles
    elif year % 100 == 0:
        return False


def is_leap_year_v3(year):
    # Toutes les années divisibles par 400 sont bissextiles
    if year % 400 == 0:
        return True
    # Toutes les années divisibles par 100 mais pas par 400 ne sont pas bissextiles
    elif year % 100 == 0:
        return False
    # Toutes les années divisibles par 4 mais pas par 100 sont bissextiles
    elif year % 4 == 0:
        return True
    # Toutes les années non divisibles par 4 ne sont pas bissextiles
    else:
        return False

class LeapTestsV1(TestCase):

    def test_year_by_400(self):
        self.assertTrue(is_leap_year_v1(1200))

    def test_year_not_by_400(self):
        self.assertFalse(is_leap_year_v1(1201))


class LeapTestsV2(TestCase):

    def test_year_by_400(self):
        self.assertTrue(is_leap_year_v2(1200))

    def test_year_not_by_400(self):
        self.assertFalse(is_leap_year_v2(1201))

    def test_year_not_by_400_but_100(self):
        self.assertFalse(is_leap_year_v2(1100))

    # Toutes les années divisibles par 4 mais pas par 100 sont bissextiles
    def test_year_by_4_but_not_by_100(self):
        self.assertTrue(is_leap_year_v2(40))


class LeapTestsV3(TestCase):

    def test_year_by_400(self):
        self.assertTrue(is_leap_year_v3(1200))

    def test_year_not_by_400(self):
        self.assertFalse(is_leap_year_v3(1201))

    def test_year_not_by_400_but_100(self):
        self.assertFalse(is_leap_year_v3(1100))

    def test_year_by_4_but_not_by_100(self):
        self.assertTrue(is_leap_year_v3(40))

    # Toutes les années non divisibles par 4 ne sont pas bissextiles
    def test_year_not_by_4(self):
        self.assertFalse(is_leap_year_v3(39))
